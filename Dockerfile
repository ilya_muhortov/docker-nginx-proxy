
FROM ubuntu:xenial

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        nginx \
        openssl \
        vim \
        wget \
    && apt-get clean \
    && rm -rf \
        /tmp/* \
        /var/lib/apt/lists/* \
        /var/tmp/*

EXPOSE 80 443

RUN rm /etc/nginx/sites-enabled/default

ADD nginx.conf /etc/nginx/nginx.conf
ADD ./nginx-conf/* /etc/nginx/sites-enabled/
ADD ./nginx-certs/* /etc/nginx/certs/

CMD ["nginx", "-g", "daemon off;"]

