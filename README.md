
```sh
# vim .env

DOCKER_RESTART=no

DOCKER_NETWORK=unity-network
```

```sh
# vim docker-compose.override.yml

version: '3'

services:
  nginx-proxy:
    external_links:
      - "name:alias"
```
